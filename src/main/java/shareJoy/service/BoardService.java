package shareJoy.service;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shareJoy.dto.Board;
import shareJoy.dto.Result;
import shareJoy.mapper.BoardImageAttachMapper;
import shareJoy.mapper.BoardMapper;
import shareJoy.mapper.ImageAttachMapper;

@Service
public class BoardService {
	
	public static final Result titleError = new Result(false,"제목을 입력해주세요.");
	public static final Result bodyError = new Result(false,"내용을 입력해주세요.");
	
	@Autowired
	BoardMapper boardMapper;
	@Autowired
	BoardImageAttachMapper boardImageAttachMapper;
	
	public Board selectById(int id){
		return boardMapper.selectById(id);
	}
	
	public List<Board> selectByCount(int count){
		return boardMapper.selectByCount(count);
	}
	
	public void delete(int id){
		boardMapper.delete(id);
	}
	
	public void create(Board board){
		boardMapper.create(board);
	}
	
	public void update(Board board){
		boardMapper.update(board);
	}
	
	/**
	 * 게시글과 관련 이미지를 한번에 삭제하기 위해 board의 body값에서 정규식을 이용해 imageAttachId를 빼온 후 boardimageAttach 데이터베이스에 boardId와 imageAttachId를 저장
	 */
	public void insertBoardImageAttach(Board board){
		String pattern = "/image/([0-9]+)/imageDownload";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(board.getBody());
		while(m.find()){
			boardImageAttachMapper.insert(board.getBoard_id(), Integer.parseInt(m.group(1)));
		}
	}
}
