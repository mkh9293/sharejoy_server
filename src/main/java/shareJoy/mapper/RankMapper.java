package shareJoy.mapper;

import shareJoy.dto.Rank;

public interface RankMapper {
	int registerRank(Rank rank);
}
