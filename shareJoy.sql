create table Member(
	member_id int AUTO_INCREMENT not null primary key,
	memberId varchar(20) not null,
	password varchar(500) not null,
	gender varchar(5) not null,
	rank_id int not null,
	constraint FK_rankId_rank foreign key(rank_id) references Rank(rank_id) ON DELETE CASCADE);
	
create table Rank(
	rank_id int auto_increment not null primary key,
	rankId varchar(10) not null);

create table Board(
	board_id int auto_increment not null primary key,
	member_id int not null,
	title varchar(50) not null,
	times varchar(50) not null,
	body Text,
	likeCount int,
	constraint FK_memberId_member foreign key(member_id) references Member(member_id) ON DELETE CASCADE);

create table BoardLike(
	boardLike_id int auto_increment not null primary key,
	board_id int not null,
	member_id int not null,
	constraint FK_boardId_board foreign key(board_id) references Board(board_id),
	constraint FK_memberId_member_boardLike foreign key(member_id) references Member(member_id) 
)
create table imageAttach(
	imageAttach_id int auto_increment not null primary key,
	imageAttach_name varchar(100) not null
);
create table boardImageAttach(
	board_id int not null,
	imageAttach_id int not null,
	constraint FK_boardId_board foreign key(board_id) references Board(board_id),
	constraint FK_imageAttachId_imageAttach foreign key(imageAttach_id) references imageAttach(imageAttach_id)
);
alter table boardAttach drop column newFileName;


drop table rank
drop table member
drop table board
drop table imageAttach;
drop table boardattach;
select * from Member;
select * from rank;
select * from board;
select * from boardLike;
select * from boardAttach;
delete from boardAttach;
select * from imageAttach;
select * from boardimageAttach;
delete from imageAttach;
delete from board;

DBCC CHECKIDENT([Board], RESEED, -1)
select IDENT_CURRENT('Rank')

SELECT TOP 0 b.board_id, b.title, b.times, b.likeCount, m.memberId
FROM [Board] b LEFT JOIN [Member] m 
ON b.member_id = m.member_id

SELECT b.board_id, b.title, b.times, b.likeCount, m.memberId
FROM [Board] b LEFT JOIN [Member] m ON b.member_id = m.member_id
Limit 0,10

INSERT Board (member_id,title,times,body,likeCount)
        VALUES (2, 'TEST', NOW(),'TEST',1)
        
delete from boardAttach where boardAttach_id in (54);

show tables;