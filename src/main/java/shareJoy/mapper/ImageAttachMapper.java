package shareJoy.mapper;

import org.apache.ibatis.annotations.Param;

import shareJoy.dto.ImageAttach;

public interface ImageAttachMapper {
	void insert(ImageAttach imageAttach);
	int delete(@Param("idList") int[] idList);
}
