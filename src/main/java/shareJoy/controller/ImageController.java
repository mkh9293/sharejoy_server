package shareJoy.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import shareJoy.dto.Result;
import shareJoy.service.BoardService;
import shareJoy.service.ImageService;

@Controller
@RequestMapping("/image")
public class ImageController {

	@Autowired
	ImageService imageService;
	
	@RequestMapping(value="/imageUpload",method=RequestMethod.POST)
	public @ResponseBody Result imageUpload(@RequestParam("file") MultipartFile uploadFile) throws IOException{
		Result result = imageService.imageUpload(uploadFile);
		if(result != null){
			return new Result(true,result.getMessage());
		}
		return Result.FAILURE;
	}
	
	@RequestMapping(value="/{id}/imageDownload",method=RequestMethod.GET)
	public void imageDonwload(@PathVariable("id")int id,HttpServletResponse response) throws IOException{
		imageService.imageDonwload(id,response);
	}
	
	@RequestMapping(value="/{idList}/imageDelete",method=RequestMethod.GET)
	public @ResponseBody Result imageDelete(@PathVariable int[] idList){
		Result result = imageService.imageDelete(idList);
		if(result != null){
			return new Result(true,result.getMessage());
		}
		return Result.FAILURE;
	}
	
	
}
