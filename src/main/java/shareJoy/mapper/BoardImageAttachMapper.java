package shareJoy.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface BoardImageAttachMapper {
	void insert(@Param("board_id")int boardId,@Param("imageAttach_id")int ImageAttachId);
	int[] selectIdByboardId(int id);
	void delete(int id);
}
