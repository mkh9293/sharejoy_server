package shareJoy.mapper;

import shareJoy.dto.Member;

public interface MemberMapper {
	void signup(Member member);
	Member selectByLoginId(String memberId);
}
