package shareJoy.dto;

public class ImageAttach {
	private int imageAttachId;
	private String fileName;

	public int getImageAttachId() {
		return imageAttachId;
	}
	public void setImageAttachId(int imageAttachId) {
		this.imageAttachId = imageAttachId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
