package shareJoy.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import shareJoy.dto.Board;
import shareJoy.dto.Result;
import shareJoy.mapper.BoardImageAttachMapper;
import shareJoy.service.BoardService;
import shareJoy.service.ImageService;

@Controller
@RequestMapping("/board")
public class BoardController {
	
	@Autowired
	BoardService boardService;
	@Autowired
	ImageService imageService;
	@Autowired
	BoardImageAttachMapper boardImageAttachMapper;
	
	@RequestMapping(value="/create",method=RequestMethod.POST)
	public @ResponseBody Result create(Board board){
		if(StringUtils.isBlank(board.getTitle())) return boardService.titleError;
		if(StringUtils.isBlank(board.getBody())) return boardService.bodyError;
		
		if(board.getBoardType()==0) boardService.create(board);
		else boardService.update(board);
		boardService.insertBoardImageAttach(board);
		Result result = Result.SUCCESS;
		result.setIndex(board.getBoard_id());
		return result;
	}
	
	@RequestMapping(value="/view",method=RequestMethod.GET)
	public @ResponseBody Board view(@RequestParam("id")int id){
		return boardService.selectById(id);
	}
	
	@RequestMapping(value="/viewList",method=RequestMethod.GET)
	public @ResponseBody List<Board> viewList(@RequestParam("count")int count){
		return boardService.selectByCount(count);
	}
	
	@RequestMapping(value="/delete",method=RequestMethod.GET)
	public @ResponseBody Result delete(@RequestParam("id")int id, @RequestParam("userId")int userId){
		Board board = boardService.selectById(id);
		
		if(board.getMember_id() != userId) return new Result(false,"권한이 없습니다.");
		
		int[] imageIdx = boardImageAttachMapper.selectIdByboardId(id);
//		for(int i : imageIdx){
//			imageService.imageDelete(i);
//		}
		imageService.imageDelete(imageIdx);
		boardImageAttachMapper.delete(id);
		boardService.delete(id);
		return Result.SUCCESS;
	}

}
