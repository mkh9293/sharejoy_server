package shareJoy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import shareJoy.dto.News;
import shareJoy.service.NewsService;

@Controller
@RequestMapping("/news")
public class NewsController {

		@Autowired
		NewsService newsService;

		@RequestMapping(value="/newsList",method=RequestMethod.POST)
		public @ResponseBody List<News> newsList(@RequestParam("searchNews")String searchNews){
			return newsService.loadNewsList(searchNews);
		}
}