package shareJoy.dto;

public class Result {
	public static final Result SUCCESS = new Result(true,"성공");
	public static final Result FAILURE = new Result(false,"실패");
	
	String message;
	boolean success;
	int index;
	
	
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Result(boolean success){
		this.success = success;
	}
	
	public Result(boolean success,String message){
		this.success = success;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public static Result getSuccess() {
		return SUCCESS;
	}

	public static Result getFailure() {
		return FAILURE;
	}
	
}
