package shareJoy.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import shareJoy.dto.Member;
import shareJoy.mapper.MemberMapper;
import shareJoy.service.MemberService;

@Controller
@RequestMapping("/member")
public class MemberController {
	
	@Autowired
	MemberService memberService;
	@Autowired
	MemberMapper memberMapper;
	
	@RequestMapping(value="/signup",method={RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Map<String,Object> signup(Member member,HttpServletResponse response){
		String msg = memberService.signupMember(member);
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("msg", msg);
		return map;
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public @ResponseBody Member login(@RequestParam("loginId")String loginId, @RequestParam("password")String password,HttpServletResponse response){
		if(loginId == null || password == null) return null;
		Member member = memberMapper.selectByLoginId(loginId);
		if(member == null) return null;
		if(member.getPassword().equals(password)) return member;
		return null;
	}
	
}
