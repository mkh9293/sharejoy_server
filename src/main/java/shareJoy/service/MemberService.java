package shareJoy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import shareJoy.dto.Member;
import shareJoy.dto.Rank;
import shareJoy.mapper.MemberMapper;
import shareJoy.mapper.RankMapper;

@Service
public class MemberService {
	
	@Autowired
	RankMapper rankMapper;
	@Autowired
	MemberMapper memberMapper;
	
	public String signupMember(Member member){
		String memCheck = member.getMemberId();
		
		if(memCheck==null || memCheck.isEmpty())
			return "아이디를 입력하세요.";
		
		memCheck = member.getPassword();
		if(memCheck==null || memCheck.isEmpty())
			return "비밀번호를 입력하세요.";
		
		memCheck = member.getPasswordCheck();
		if(memCheck==null || memCheck.isEmpty())
			return "비밀번호 확인을 입력하세요.";

		memCheck = member.getGender();
		if(memCheck==null || memCheck.isEmpty())
			return "성별을 선택하세요.";
		
		Rank rank = new Rank();
		rank.setRankId("일반");
		rankMapper.registerRank(rank);
		member.setRank_id(rank.getRank_id());
		memberMapper.signup(member);
		
		return "회원가입 성공";
	}
}
