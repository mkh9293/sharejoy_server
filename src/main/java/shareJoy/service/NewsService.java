package shareJoy.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Service;

import shareJoy.dto.News;

@Service
public class NewsService {
	
	public List<News> loadNewsList(String searchNews){
		String line = "";
		try {
			URLConnection conn;
			
			if(searchNews.equals(""))
				conn = new URL("http://newssearch.naver.com/search.naver?where=rss&query="+URLEncoder.encode("화제","UTF-8")).openConnection();
			else
				conn = new URL("http://newssearch.naver.com/search.naver?where=rss&query="+URLEncoder.encode(searchNews,"UTF-8")).openConnection();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
			
			line = br.readLine();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return parseNews(line);
	}
	
	public List<News> parseNews(String newsLine){
		List<News> newsList = new ArrayList<News>();
		SimpleDateFormat original_format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z",Locale.UK);
		SimpleDateFormat new_format = new SimpleDateFormat("yy년 MM월 dd일 HH:mm"); 
		Date original_date;
		try{
			SAXReader saxReader = new SAXReader();
			Document document = saxReader.read(new StringReader(newsLine));
			List<Node> nodes = document.selectNodes("/rss/channel/item");
			
			int i = 0; // 뉴스 기사의 갯수를 10개로 제한
			
			for(Node node : nodes){
				if((i++) <= 10){
					News news = new News();
					news.setTitle(node.selectSingleNode("title").getText());
					news.setLink(node.selectSingleNode("link").getText());
					news.setContents(node.selectSingleNode("description").getText());
					//날짜 형식 변환 후 객체에 저장
					original_date = original_format.parse(node.selectSingleNode("pubDate").getText());
					news.setWriteTime(new_format.format(original_date));
					news.setImgPath(node.selectSingleNode("media:thumbnail").valueOf("@url"));
					newsList.add(news);
				}
			}
		}catch(Exception e){
		}
		return newsList;
	}
	
	
}
