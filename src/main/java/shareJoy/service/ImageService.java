package shareJoy.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import shareJoy.dto.ImageAttach;
import shareJoy.dto.Result;
import shareJoy.mapper.ImageAttachMapper;

@Service
public class ImageService {
	
	private final static String PATH = "c:\\uploadImg\\";
	
	@Autowired
	ImageAttachMapper imageAttachMapper;
	
	public Result imageUpload(MultipartFile uploadFile){
		if(uploadFile.getSize() > 0){
			String fname = uploadFile.getOriginalFilename().toString();
			ImageAttach imageAttach = new ImageAttach();
			imageAttach.setFileName(fname);
			imageAttachMapper.insert(imageAttach);
			File f = new File(PATH+imageAttach.getImageAttachId()+".jpg");

			try{
				uploadFile.transferTo(f);
			}catch(IOException e){}
			return new Result(true,String.valueOf(imageAttach.getImageAttachId()));
		}
		return null;
	}
	
	public void imageDonwload(int id,HttpServletResponse response) throws IOException{
		File f = new File(PATH+id+".jpg");
		response.setContentType("image/jpeg");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.setHeader("Content-Disposition", "filename="+f.getName()+";");
		byte[] bytes = readFile(f.getName().toString());
		try(BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream())){
			output.write(bytes);
		}
	}

	//file형 데이터를 byte형태로 변환
	private byte[] readFile(String fileName) throws IOException {
		String path = PATH+fileName;
		
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(path));
		int length = bis.available();
		byte[] bytes = new byte[length];
		bis.read(bytes);
		bis.close();
		
		return bytes;
	}
	
	public Result imageDelete(int[] ids){
		String path = ""; 
		for(int id : ids){
			path = PATH + id + ".jpg";
			System.out.println("path= "+path);
			//트랜잭션 추가해야함 현재는 추가하지 않았음.
			File f = new File(path);
			if(f.delete())
				System.out.println(id+" 삭제를 성공하였습니다.");
			else{ 
				System.out.println(id+" 삭제를 실패하였습니다.");
				return null;
			}
		}
		imageAttachMapper.delete(ids);
		return new Result(true,"파일 삭제 성공");
	}
	
//	public Result imageDelete(int id){
//		if(imageAttachMapper.delete(id) != 1){
//			return Result.FAILURE;
//		}
//		File f = new File("c:\\uploadImg\\"+id+".jpg");
//		if(f.delete())
//			return Result.SUCCESS;
//		else
//			return Result.FAILURE;	
//	}
	
}
