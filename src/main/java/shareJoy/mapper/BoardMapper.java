package shareJoy.mapper;

import java.util.List;

import shareJoy.dto.Board;

public interface BoardMapper {
	int create(Board board);
	Board selectById(int id);
	List<Board> selectByCount(int count);
	int update(Board board);
	void delete(int id);
}
